# Test Fixtures

The fixtures folder is meant to hold test files that can be used to create positive and negative tests.

The test files should be organized in a folder per _test.go file.   The folder should be named after the test.  For example foo_test.go 
should use a folder called foo.  Sub folders are fine. Just don't go nuts and KISS 