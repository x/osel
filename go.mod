module "git.openstack.org/openstack/osel"

require (
	"github.com/fsnotify/fsnotify" v1.4.7
	"github.com/google/go-querystring" v0.0.0-20170111101155-53e6ce116135
	"github.com/hashicorp/hcl" v0.0.0-20171017181929-23c074d0eceb
	"github.com/magiconair/properties" v1.7.6
	"github.com/mitchellh/mapstructure" v0.0.0-20180220230111-00c29f56e238
	"github.com/nate-johnston/viper" v1.0.1
	"github.com/pelletier/go-toml" v1.1.0
	"github.com/racker/perigee" v0.1.0
	"github.com/rackspace/gophercloud" v1.0.0
	"github.com/spf13/afero" v1.0.2
	"github.com/spf13/cast" v1.2.0
	"github.com/spf13/pflag" v1.0.0
	"github.com/streadway/amqp" v0.0.0-20180131094250-fc7fda2371f5
	"golang.org/x/sys" v0.0.0-20180302081741-dd2ff4accc09
	"golang.org/x/text" v0.0.0-20171214130843-f21a4dfb5e38
	"gopkg.in/yaml.v2" v1.1.1-gopkgin-v2.1.1
)
